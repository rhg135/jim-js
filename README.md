# jschat

A Clojurescript library callable from JS to interface with the [JIM](https://jim.hackpad.com/) protocol.
Of course this has extensions to the protocol for mass-chat.

## Usage

TBD

## License

jim-js Copyright © 2013 Ricardo Gomez

This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it
under certain conditions
