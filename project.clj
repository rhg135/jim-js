(defproject jim-js "0.0.1-SNAPSHOT"
  :description "a JS library to chat over jim"
  :url "https://bitbucket.org/rhg135/jim-js"
  :license {:name "General Public License v3"
            :url "http://www.gnu.org/licenses/gpl.txt"}
  :plugins [[lein-cljsbuild "0.3.3"]]
  :cljsbuild {
              :builds [{
                        :source-paths ["cljs"]
                        :compiler {
                                   :output-to "target/js/main.js"
                                   :optimizations :simple
                                   :target :nodejs
                                   :pretty-print true}}]}
:dependencies [[org.clojure/clojurescript "0.0-1934"]
                [org.clojure/clojure "1.5.1"]])
