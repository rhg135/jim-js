;jim-js: a library for implementing jim clients/servers
;Copyright (C) 2013 Ricardo Gomez
;
;This program is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program.  If not, see <http://www.gnu.org/licenses/>.
(ns jim-js.core
  (:require [cljs.nodejs :as node]))

(def net (node/require "net"))

(defn connect
  "Opens a connection to a server
  Returns a map"
  ([host port]
   {:socket (.connect net {:host host
                       :port port})}))

(defn read-byte!
  "Reads a byte from a stream
  WARNING MUTATION"
  ([s]
   (.read s 1)))

(defn read-json!
  "Reads a json object from a stream"
  ([s]
    (loop [s s
           json (read-byte! s)]
      (try
        (.parse js/JSON jsons)
        (catch js/SyntaxError _
          (recur s (str s (read-byte! s))))))))

(defn read-jim
  "Takes a jim map and returns the next json object"
  ([jim]
   (alter jim assoc :socket (read-json! (:socket @jim)))))

(defn write-jim
  "Writes an object to a jim"
  ([jim o]
   (.write (:socket @jim) (.stringify js/JSON o))))

(defn open
  "Opens a connection to a jim server and returns a vector of the map and status"
  ([host port]
   (let [jim (ref (connect host port))
         obj (js->clj (read-jim jim))]
     (alter jim assoc :status (case (:response obj)
                                200 :ok
                                401 :authenticate
                                :else (assert "should not happen"))))))

(defn authenticate
  "Sends an authentication request to a jim server"
  ([jim user pass]
   (let [req {:action :authenticate
              :user {:account_name user
                     :password pass}}]
     (write-jim jim req)
     (when (= (:response (read-jim jim)) 200) 
       :success))))

(defn start
  "A simple way to test this"
  ([& _]
   (let [jim (open "127.0.0.1" 4004)]
     (println "Success"))))

(set! *main-cli-fn* start)
